﻿using TallerTres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListarPersonas : ContentPage
    {
        public ListarPersonas()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {

                List<Personas> listaTareas;
                listaTareas = connection.Table<Personas>().ToList();

                listaPersonas.ItemsSource = listaTareas;

            }
        }
    }
}