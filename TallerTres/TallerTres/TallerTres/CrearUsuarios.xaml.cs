﻿using TallerTres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearUsuarios : ContentPage
	{
		public CrearUsuarios ()
		{
			InitializeComponent ();
		}

        public void guardarUsu(object sender, EventArgs e)
        {

            int select = Picker.SelectedIndex;

            if (string.IsNullOrEmpty(entryusuario.Text) || string.IsNullOrEmpty(entrypassword.Text) || string.IsNullOrEmpty(entryavatar.Text) || (select == -1))
            {


                DisplayAlert("Error", "Debe Escribir Datos completos", "Ok");

            }
            else
            {

                // cre entero que me guarda la posicion del Picker

                int selected = Picker.SelectedIndex;

                // crear objeto del modelo tarea
                Usuarios Usuar = new Usuarios()
                {
                    nombre_usuario = entryusuario.Text,
                    password = entrypassword.Text,
                    avatar = entryavatar.Text,
                    estado = Picker.Items[selected]
                    //estado = Picker.SelectedItemProperty.ToString(),
                };

                // conexion a la base de datos
                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
                {
                    // crear tabla en base de datos
                    connection.CreateTable<Usuarios>();

                    // crear registro en la tabla
                    var result = connection.Insert(Usuar);

                    if (result > 0)
                    {
                        DisplayAlert("Correcto", "El Usuario se creo correctamente", "OK");
                    }
                    else
                    {
                        DisplayAlert("Incorrecto", "El usuario no fue creada", "OK");
                    }
                }
                entryusuario.Text = "";
                entrypassword.Text = "";
                entryavatar.Text = "";
                Picker.SelectedIndex = -1;
            }
        }

        async public void listarUsu(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarUsuarios());
        }


    }
}