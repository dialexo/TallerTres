﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTres.Models
{
    class Usuarios
    {

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string nombre_usuario { get; set; }

        public string password { get; set; }

        public string avatar { get; set; }

        public string estado { get; set; }
    }
}
