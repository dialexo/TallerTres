﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTres.Models
{
    class Personas
    {

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string nombre { get; set; }

        public string telefono { get; set; }

        public string email { get; set; }

        public string genero { get; set; }


    }
}
