﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TallerTres
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async private void ingresPersona(object sender, EventArgs e)
        {

            await DisplayAlert("Correcto", "Ingreso a Persona ", "Ok");
            await Navigation.PushAsync(new CrearPersonas());
        
        }

        async private void ingresUsuario(object sender, EventArgs e)
        {

            await DisplayAlert("Correcto", "Ingreso a Usuario ", "Ok");
            await Navigation.PushAsync(new CrearUsuarios());

        }

    }
}
