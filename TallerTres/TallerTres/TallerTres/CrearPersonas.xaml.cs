﻿using TallerTres.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearPersonas : ContentPage
	{           
		public CrearPersonas ()
		{
			InitializeComponent ();
		}

        //
        public void GuardarPer(object sender, EventArgs e)
        {
            int select = Picker.SelectedIndex;

            if (string.IsNullOrEmpty(entrynombre.Text) || string.IsNullOrEmpty(entrytelefono.Text) || string.IsNullOrEmpty(entryemail.Text) || (select==-1))
            {


                 DisplayAlert("Error", "Debe Escribir Datos completos", "Ok");

            }
            else
            {


                // cre entero que me guarda la posicion del Picker

                int selected = Picker.SelectedIndex;

                // crear objeto del modelo tarea
                Personas person = new Personas()
                {
                    nombre = entrynombre.Text,
                    telefono = entrytelefono.Text,
                    email = entryemail.Text,
                    genero = Picker.Items[selected]
                };

                // conexion a la base de datos
                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
                {
                    // crear tabla en base de datos
                    connection.CreateTable<Personas>();

                    // crear registro en la tabla
                    var result = connection.Insert(person);

                    if (result > 0)
                    {
                        DisplayAlert("Correcto", "La persona se creo correctamente", "OK");
                    }
                    else
                    {
                        DisplayAlert("Incorrecto", "La persona no fue creada", "OK");
                    }
                }

                entrynombre.Text = "";
                entrytelefono.Text = "";
                entryemail.Text = "";
                Picker.SelectedIndex = -1;
            }
        }
        //
        async public void listarPer(object sender, EventArgs e)
        {        
            await Navigation.PushAsync(new ListarPersonas());
        }      
    }
}